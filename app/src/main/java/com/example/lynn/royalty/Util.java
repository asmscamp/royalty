package com.example.lynn.royalty;

import android.content.Context;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

/**
 * Created by lynn on 2/24/2018.
 */

public class Util {

    public static Point getCenter(Context context) {
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        Display display = windowManager.getDefaultDisplay();

        Point output = new Point();

        display.getSize(output);

        output.x /= 2;
        output.y /= 2;

        return(output);
    }

    public static int getHeight(Context context) {
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        Display display = windowManager.getDefaultDisplay();

        Point output = new Point();

        display.getSize(output);

        return(output.y);
    }


    public static int getWidth(Context context) {
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);

        Display display = windowManager.getDefaultDisplay();

        Point output = new Point();

        display.getSize(output);

        return(output.x);
    }

}
