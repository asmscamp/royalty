package com.example.lynn.royalty;

import android.graphics.Color;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;

import static com.example.lynn.royalty.MainActivity.*;

/**
 * Created by lynn on 2/25/2018.
 */

public class MyListener implements View.OnTouchListener {
    private float offSetX;
    private float offSetY;

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)v.getLayoutParams();

        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            offSetX = event.getRawX() - layoutParams.leftMargin;
            offSetY = event.getRawY() - layoutParams.topMargin;
        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
            layoutParams.leftMargin = (int)(event.getRawX() - offSetX);
            layoutParams.topMargin = (int)(event.getRawY() - offSetY);

            v.setLayoutParams(layoutParams);

            Rect rectangle = new Rect(layoutParams.leftMargin,layoutParams.topMargin,
                    layoutParams.leftMargin + v.getWidth(),layoutParams.topMargin + v.getHeight());

            output.setText(rectangle.toString());

            for (int counter=0;counter<rectangles.length;counter++)
                if (rectangles[counter].contains(rectangle)) {
                    colors[counter] = Color.argb(255, 255, 0, 0);
                } else
                    colors[counter] = Color.argb(255,255,255,255);

            myView.invalidate();
        }

        return(true);
    }

}
