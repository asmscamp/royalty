package com.example.lynn.royalty;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Arrays;

import static com.example.lynn.royalty.MainActivity.*;

/**
 * Created by lynn on 2/22/2018.
 */

public class MyView extends RelativeLayout {
    private Paint paint;

    public MyView(Context context) {
        super(context);

        paint = new Paint();

        paint.setColor(0xFF000000);

        setWillNotDraw(false);

        button = new Button(context);

        addView(button);

        button.setOnTouchListener(listener);

        colors = new int[10];

        for (int counter=0;counter<colors.length;counter++)
            colors[counter] = Color.argb(255,255,255,255);

        output = new TextView(context);

        output.setTextSize(40);

        scrollView = new ScrollView(context);

        scrollView.addView(output);

        addView(scrollView);


    }

    public void onDraw(Canvas canvas) {
        Point center = Util.getCenter(getContext());

        int totalWidth = Util.getWidth(getContext());

        int totalHeight = Util.getHeight(getContext());

        int widthOfRectangle = (int)(0.5*totalWidth/2);

        canvas.drawRect(center.x - widthOfRectangle/2,0,center.x - widthOfRectangle/2 - 5,totalHeight,paint);

        canvas.drawRect(center.x + widthOfRectangle/2,0,center.x + widthOfRectangle/2 + 5,totalHeight,paint);

        int heightOfRectangle = totalHeight/10 - (totalHeight/10)/10;

        paint.setStyle(Paint.Style.STROKE);

        paint.setStrokeWidth(5.0f);

        if (rectangles == null) {
            rectangles = new Rect[10];

            for (int counter=0;counter<10;counter++)
               rectangles[counter] = new Rect(center.x - widthOfRectangle/2,heightOfRectangle*counter,center.x + widthOfRectangle/2 + 5,heightOfRectangle*(counter + 1));

        }

        System.out.println(Arrays.toString(colors));

        for (int counter=0;counter<10;counter++)
            canvas.drawRect(center.x - widthOfRectangle/2,heightOfRectangle*counter,center.x + widthOfRectangle/2 + 5,heightOfRectangle*(counter + 1),paint);

        for (int counter=0;counter<10;counter++) {
            Paint paint = new Paint();

            paint.setColor(colors[counter]);

            Rect rectangle = new Rect(rectangles[counter].left + 5,rectangles[counter].top + 5,rectangles[counter].right - 5,rectangles[counter].bottom - 5);

            canvas.drawRect(rectangle,paint);
        }

    }

}
