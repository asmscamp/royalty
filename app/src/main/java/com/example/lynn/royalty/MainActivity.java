package com.example.lynn.royalty;

import android.graphics.Rect;
import android.support.constraint.solver.widgets.Rectangle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    public static Button button;
    public static MyListener listener;
    public static Rect[] rectangles;
    public static int[] colors;
    public static MyView myView;
    public static TextView output;
    public static ScrollView scrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        listener = new MyListener();

        setContentView(myView = new MyView(this));
    }
}
